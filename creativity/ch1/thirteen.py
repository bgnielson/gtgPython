#!/usr/bin/python3

#take the list
#find length of list

#make a new list
#for item in old list
    #newList[i] = oldList[-i]

#use the reverse method
mylist = [1, 2, 3, 4, 5]
mylist.reverse()
print(mylist)

#slice it
mylist = [1, 2, 3, 4, 5]
mylist[::-1]
print(mylist)

#reverse loop
mylist = [1, 2, 3, 4, 5]
for item in reversed(mylist):
    print(item)
