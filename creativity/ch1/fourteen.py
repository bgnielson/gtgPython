#!/usr/bin/python3



def is_even(k):
    even = True
    i = 0

    while i < abs(k):
        i += 1
        even = not even
    return even

