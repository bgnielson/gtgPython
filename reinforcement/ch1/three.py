#!/usr/bin/python

#returns the smallest and biggest numbers of a sequence
def minmax(data):
    biggest = data[0]
    smallest = data[0]

    for y in data:
        if y < smallest:
            smallest = y

    for x in data:
        if x > biggest:
            biggest = x

    return smallest, biggest

#print(minmax([5,9,2,3,4]))
