#!/usr/bin/python3
import two

def oddsquares(data):
    return sum(x*x for x in range(1,data-1) if two.is_even(x) is False)

#tests oddsquares
print(oddsquares(5))
