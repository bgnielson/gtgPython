#!/usr/bin/python3

# A short Python function that takes an integer value and
# returns True if k is even, and False otherwise

#determines if an integer is even
def is_even(k):
    even = True
    i = 0

    while i < abs(k):
        i += 1
        even = not even
    return even

#tests is_even
#print(is_even(9))
