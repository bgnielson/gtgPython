#!/usr/bin/python3
import two

def oddsquares(data):
    sums = 0
    for x in range(1,data-1):
        if two.is_even(x) is False:
            sums += x*x
    return sums

#tests oddsquares
print(oddsquares(5))
