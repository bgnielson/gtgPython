#!/usr/bin/python3

# A short python function that takes two integer values
# and returns True if n is a multiple of m, that is,
# n = mi for some integer i, and False otherwise

#determines if n is a multiple of m
def is_multiple(n, m):
    if n % m is 0:
        return True
    else:
        return False

print(is_multiple(12, 9))
