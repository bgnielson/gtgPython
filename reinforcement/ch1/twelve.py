#!/usr/bin/python3

from random import randrange


def myChoice(data):
   listOfData = []

   for i in data:
       listOfData.append(i)

   theChoice = randrange(len(listOfData))

   return listOfData[theChoice]

someData = [3, 4, 7, 18]
print( myChoice(someData) )
