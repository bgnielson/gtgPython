#!/usr/bin/python3

#for n, returns the sum of the squares of all positive ints smaller than n
def sumsquaressmaller(data):
    sums = 0
    for x in range(1,data-1):
        sums += x*x
    return sums

#tests sumsquaressmaller
print(sumsquaressmaller(7))
