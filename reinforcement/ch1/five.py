#!/usr/bin/python3

def sumsquaressmaller(data):
    return sum(x*x for x in range(1,data-1))

#tests sumsquaressmaller
print(sumsquaressmaller(7))
